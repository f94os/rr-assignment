package com.rr.assignment.function;

/**
 * @author osandstrom
 * Date: 2014-05-07 Time: 16:08
 */
public interface Mapper<S, T> {
  T apply(S source);
}
