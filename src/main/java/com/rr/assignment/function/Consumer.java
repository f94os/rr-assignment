package com.rr.assignment.function;

/**
 * @author osandstrom
 * Date: 2014-05-07 Time: 16:06
 */
public interface Consumer<T> {
  void accept(T t);
}
