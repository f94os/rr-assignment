package com.rr.assignment;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @author osandstrom
 * Date: 2014-05-07 Time: 16:12
 */
public class Specific {

  public Collection<String> reachOut(Iterable<Person> members) {
    ArrayList<String> target = new ArrayList<String>();
    for (Person p : members) {
      if (21 <= p.getAge() && p.getAge() < 28) {
        target.add(p.getEmail());
      }
    }
    return target;
  }

}
