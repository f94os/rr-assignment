package com.rr.assignment;

import static com.rr.assignment.NameParser.getFirstName;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

/**
 */
public class NameParserTest {

  @Test
  public void testGetFirstName() {
    assertNull(getFirstName(null));
    assertEquals("James", getFirstName("James"));
    assertEquals("James", getFirstName("James Brown"));
    assertEquals("James", getFirstName("Brown, James"));

    assertEquals("James Lewis", getFirstName("James Lewis Brown"));
    assertEquals("James", getFirstName("James van Brown"));
  }
}
